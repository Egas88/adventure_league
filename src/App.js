import { BrowserRouter, Route, Routes } from "react-router-dom"
import './App.css';
import LoginSignup from './components/Login_signup/LoginSignup';
import useToken from './components/useToken'
import SideBar from "./components/SideBar/SideBar";


function App() {
  return (
      <div className="App">
        <div className="nav_bar">
          
        </div>
        <div className="side_bar">
          <SideBar/>
        </div>
        <main className="main_page">

        </main>
      </div>
  );
}

export default App;
