import React from 'react'

import { BarChart, Compass, Layout, List } from "lucide-react";
import { Link } from "react-router-dom";

function SidebarRoutes() {

  const guestRoutes = [
    {
      icon: Layout,
      label: "Dashboard",
      href: "/",
    },
    {
      icon: Compass,
      label: "Browse",
      href: "/search",
    },
  ];


  const teacherRoutes = [
    {
      icon: List,
      label: "Courses",
      href: "/teacher/courses",
    },
    {
      icon: BarChart,
      label: "Analytics",
      href: "/teacher/analytics",
    },
  ]



  return (
    <div>SidebarRoutes</div>
  )
}

export default SidebarRoutes