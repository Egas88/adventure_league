import svg from "./logo.svg";

export const Logo = () => {
    return (
      <img
        width={130}
        height={31.34}
        decoding="async"
        loading="lazy"
        alt="logo"
        src={svg}
      />
    )
  }