import React from 'react'
import "./SideBar.css"
import { Logo } from './Logo/Logo'

const SideBar = (props) => {
  return (
    <div className='sidebar'>
        <div className='logo'>
          <Logo/>
        </div>
        <div className='sidebar_routes'>
            
        </div>
    </div>
  )
}

export default SideBar