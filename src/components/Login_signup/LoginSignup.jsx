import React, { useState } from 'react'
import "./LoginSignup.css"
import httpClient from "../../HttpClient"
import useToken from '../useToken'

import email_icon from './Assets/email.png'
import password_icon from './Assets/password.png'


const LoginSignup = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [action, setAction] = useState("Login");
  const { token, removeToken, setToken, userId } = useToken();

  const logInUser = async () => {
    await httpClient.post("//localhost:5000/login", {
      email,
      password
    }).then((resp) => {
      if (resp.status === 200) {
        setToken(resp.data.access_token);
        window.location.href = "/";
      } else {
        alert(resp.err);
      }
    }).catch((err) => {
      alert(err);
    });
};

const RegisterUser = async () => {
  await httpClient.post("//localhost:5000/register", {
    email,
    password
  }).then((resp) => {
    if (resp.status === 200) {
      setToken(resp.data.access_token);
      window.location.href = "/";
    } else {
      alert(resp.err);
    }
  }).catch((err) => {
    alert(err);
  });
};

  return (
    <div className='container'>
      <div className='header'>
        <div className="text">{action}</div>
        <div className="underline"></div>
      </div>
        <div className="inputs">
          <div className="input">
            <img src={email_icon} alt=''/>
            <input 
              type='email' 
              placeholder='Email'
              onChange = {(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="input">
            <img src={password_icon} alt=''/>
            <input 
              type='password' 
              onChange={(e) => setPassword(e.target.value)}
              placeholder='Password'
            />
          </div>
        </div>
        <div className="forgot-password">Lost password? <span>Click here!</span></div>
        <div className="submit-container">
          <div className={action==="Login"?"submit gray":"submit"} onClick={action === "Login" ? () => {setAction("Sign Up")} : () => logInUser()}>Sign Up</div>
          <div className={action==="Sign Up"?"submit gray":"submit"} onClick={action==="Sign Up"? ()=>{setAction("Login")} : () => RegisterUser()}>Login</div>
        </div>
    </div>
  )
}

export default LoginSignup